from django.shortcuts import render
from suerte.models import Sorteo

# Create your views here.
# from django.http import HttpResponse


# def index(request):
#     return HttpResponse("Hello, world. pages.")

def index(request):
    sorteos = Sorteo.objects.all()

    context = {
        'sorteos': sorteos,

    }
    return render(request, 'pages/index.html', context)
