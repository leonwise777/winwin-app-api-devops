from django.db import models
from datetime import datetime

from core.models import User

# Create your models here.


class Sorteo(models.Model):
    sorteo_name = models.CharField(max_length=50, blank=True)
    sorteo_image_url = models.ImageField(
        upload_to='photos/%Y/%m/%d', blank=True)
    sorteo_creation_date = models.DateTimeField(
        default=datetime.now, blank=False)
    sorteo_date = models.DateTimeField(blank=True)
    is_published = models.BooleanField(default=False, blank=True)
    number_of_entrants = models.IntegerField(blank=True)
    winner_name = models.CharField(
        max_length=50, blank=True)
    winner_url = models.CharField(
        max_length=250, default='', blank=True, null=True)


class Boletos(models.Model):
    boleto_creation_date = models.DateTimeField(
        default=datetime.now, blank=False)
    boleto_number = models.PositiveIntegerField(
        null=True, blank=True)
    user_name = models.ForeignKey(
        User, related_name='user_name', on_delete=models.CASCADE)
    boleto_sorteo_related_id = models.ForeignKey(
        Sorteo, related_name='boleto_sorteo_related_id',
        on_delete=models.CASCADE)
