from django.contrib import admin
from suerte.models import Sorteo, Boletos


class SorteoAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'sorteo_name'
    )


class BoletosAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'boleto_number'
    )


admin.site.register(Sorteo, SorteoAdmin)
admin.site.register(Boletos, BoletosAdmin)
